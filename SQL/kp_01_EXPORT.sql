-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2019 at 06:45 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kp_01`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `account_type` tinyint(4) DEFAULT NULL,
  `username` varchar(16) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_log` datetime DEFAULT NULL,
  `user_detail` int(11) NOT NULL,
  `classtype` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medical_detail`
--

CREATE TABLE `medical_detail` (
  `id` int(11) NOT NULL,
  `detail` varchar(32) NOT NULL,
  `patient` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medical_record`
--

CREATE TABLE `medical_record` (
  `id` int(11) NOT NULL,
  `patient` int(11) NOT NULL,
  `symptoms` varchar(512) NOT NULL,
  `bp_systolic` smallint(6) DEFAULT NULL,
  `bp_diastolic` smallint(6) DEFAULT NULL,
  `session` int(11) NOT NULL,
  `type_case` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medicine`
--

CREATE TABLE `medicine` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `type` varchar(32) NOT NULL,
  `shape` varchar(8) NOT NULL,
  `dose` varchar(8) NOT NULL,
  `stock` smallint(6) DEFAULT NULL,
  `unit` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `id_index` int(11) DEFAULT NULL,
  `id_bpjs` varchar(16) NOT NULL,
  `patient_detail` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `id_index`, `id_bpjs`, `patient_detail`) VALUES
(1, 1001, '1234567890001', 1),
(2, 1422, '4321567888772', 2),
(3, 4433, '3421567876593', 3),
(4, 1011, 'pu', 4),
(5, 9912, 'pu', 5),
(6, 2020, '5010432108080', 6),
(7, 2001, 'pu', 7),
(8, 6545, '6621072743210', 8),
(9, 9090, '4323432879032', 9),
(10, 3333, '6425637599432', 10),
(11, 4212, '1122334455667', 11),
(12, 9492, '9093234366864', 12),
(13, 3324, '4342666784562', 13),
(14, 6534, 'pu', 14),
(15, 6334, 'pu', 15);

-- --------------------------------------------------------

--
-- Table structure for table `patient_detail`
--

CREATE TABLE `patient_detail` (
  `id` int(11) NOT NULL,
  `fullname` varchar(64) NOT NULL,
  `date_ofbirth` date DEFAULT NULL,
  `place_ofbirth` varchar(16) NOT NULL,
  `sex` varchar(1) NOT NULL,
  `home_address` varchar(64) NOT NULL,
  `firm` varchar(32) DEFAULT NULL,
  `contact` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_detail`
--

INSERT INTO `patient_detail` (`id`, `fullname`, `date_ofbirth`, `place_ofbirth`, `sex`, `home_address`, `firm`, `contact`) VALUES
(1, 'diki adita m', '1997-11-10', 'lamongan', 'l', 'ganggang,medalem,modo-lamongan', 'driver gojek', '085706812866'),
(2, 'm aji pangestu', '1997-08-12', 'sidoarjo', 'l', 'jl.rajawali no.6, sedati-sidoarjo', 'driver gojek', '085612143612'),
(3, 'badruz zaman', '1996-06-02', 'sidoarjo', 'l', 'jl.kolonel sugiono,panjunan-sidioaro', 'pedagang lkan', '085647586000'),
(4, 'park chaeyong', '1997-06-05', 'korea', 'p', 'jl.seoul no.101-korea', 'idol', '0825276488774'),
(5, 'jihan audy', '1999-01-11', 'mojokerto', 'p', 'jl.jylo manise-mojokerto', 'penyanyi', '085653625421'),
(6, 'budi budiman', '1990-07-20', 'surabaya', 'l', 'jl.pacar no.05, genteng - surabaya', 'honda dealer', '085675328775'),
(7, 'riki pambudi', '1997-08-18', 'sidoarjo', 'l', 'buduran no.22 - sidoarjo', 'telkom', '081263236872'),
(8, 'abdul zikri', '1979-01-20', 'sidoarjo', 'l', 'jl.soekarno iv no.9 - sidoarjo', 'telkom', '08123443564'),
(9, 'nur aini', '1994-04-04', 'sidoarjo', 'p', 'perum grand lagos no.8 - sidoarjo', 'telkom', '08137465748'),
(10, 'joko sembudi harsono', '1992-02-02', 'malang', 'l', 'sandiego no. 8a - surabaya', 'wingsfood', '085256518289'),
(11, 'riziek shihab kristanto', '1993-02-01', 'gresik', 'l', 'gresik kota baru', 'semen gresik', '085643253611'),
(12, 'andin budi hapsari', '1988-08-18', 'gresik', 'p', 'jl.mengayam 8 no.1,driyorejo - gresik', 'miwon', '0812247492749'),
(13, 'abdul junaidi', '1989-09-14', 'sidoarjo', 'l', 'perum grand surya no.2 - sidoarjo', 'pedagang buah', '08137435722'),
(14, 'dewi sandra', '1980-08-09', 'surabaya', 'p', 'sandiego v no.1 - surabaya', 'kasir mall', '085767217133'),
(15, 'desi purnamasari', '1997-07-07', 'malang', 'p', 'jl. abdul kodir ii no.33', 'yamaha dealer', '081937222425');

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE `prescription` (
  `id` int(11) NOT NULL,
  `medical_record` int(11) NOT NULL,
  `medicines` int(11) NOT NULL,
  `dosage` varchar(8) NOT NULL,
  `detail` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE `queue` (
  `id` int(11) NOT NULL,
  `patient` int(11) NOT NULL,
  `queue_position` tinyint(4) DEFAULT NULL,
  `timestamp` time DEFAULT NULL,
  `session` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `shift` varchar(8) NOT NULL,
  `doctor` int(11) NOT NULL,
  `pharmacist` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `id` int(11) NOT NULL,
  `fullname` varchar(64) NOT NULL,
  `home_address` varchar(128) NOT NULL,
  `contact` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_account__user_detail` (`user_detail`);

--
-- Indexes for table `medical_detail`
--
ALTER TABLE `medical_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_medical_detail__patient` (`patient`);

--
-- Indexes for table `medical_record`
--
ALTER TABLE `medical_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_medical_record__patient` (`patient`),
  ADD KEY `idx_medical_record__session` (`session`);

--
-- Indexes for table `medicine`
--
ALTER TABLE `medicine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_patient__patient_detail` (`patient_detail`);

--
-- Indexes for table `patient_detail`
--
ALTER TABLE `patient_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prescription`
--
ALTER TABLE `prescription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_prescription__medical_record` (`medical_record`),
  ADD KEY `idx_prescription__medicines` (`medicines`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_queue__patient` (`patient`),
  ADD KEY `idx_queue__session` (`session`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_session__doctor` (`doctor`),
  ADD KEY `idx_session__pharmacist` (`pharmacist`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medical_detail`
--
ALTER TABLE `medical_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medical_record`
--
ALTER TABLE `medical_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medicine`
--
ALTER TABLE `medicine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `patient_detail`
--
ALTER TABLE `patient_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `prescription`
--
ALTER TABLE `prescription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue`
--
ALTER TABLE `queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `fk_account__user_detail` FOREIGN KEY (`user_detail`) REFERENCES `user_detail` (`id`);

--
-- Constraints for table `medical_detail`
--
ALTER TABLE `medical_detail`
  ADD CONSTRAINT `fk_medical_detail__patient` FOREIGN KEY (`patient`) REFERENCES `patient` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `medical_record`
--
ALTER TABLE `medical_record`
  ADD CONSTRAINT `fk_medical_record__patient` FOREIGN KEY (`patient`) REFERENCES `patient` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_medical_record__session` FOREIGN KEY (`session`) REFERENCES `session` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patient`
--
ALTER TABLE `patient`
  ADD CONSTRAINT `fk_patient__patient_detail` FOREIGN KEY (`patient_detail`) REFERENCES `patient_detail` (`id`);

--
-- Constraints for table `prescription`
--
ALTER TABLE `prescription`
  ADD CONSTRAINT `fk_prescription__medical_record` FOREIGN KEY (`medical_record`) REFERENCES `medical_record` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_prescription__medicines` FOREIGN KEY (`medicines`) REFERENCES `medicine` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `queue`
--
ALTER TABLE `queue`
  ADD CONSTRAINT `fk_queue__patient` FOREIGN KEY (`patient`) REFERENCES `patient` (`id`),
  ADD CONSTRAINT `fk_queue__session` FOREIGN KEY (`session`) REFERENCES `session` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `fk_session__doctor` FOREIGN KEY (`doctor`) REFERENCES `account` (`id`),
  ADD CONSTRAINT `fk_session__pharmacist` FOREIGN KEY (`pharmacist`) REFERENCES `account` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
