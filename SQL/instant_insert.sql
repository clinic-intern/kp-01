
-- medichine
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Amoxicillin Trihydrate','Antibiotik','Kaplet','500 mg',100,'Strip');
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Piroxicam','Analgetik','Tablet','20 mg',100,'Strip');
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Paracetamol','Antibiotik','Tablet','500 mg',100,'Strip');
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Novagesic Paracetamol','Antibiotik','Sirup','60 ml',100,'Botol');
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Antasida Doen','Gastrointestinal','Sirup','60 ml',100,'Botol');

-- user_detail
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Mohammad Ajiemas P.','Jl. Lurus Kampung Lurus, Sidoarjo','087812345678');
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('M. Taufan Agusta','Jl. Belok Kampung Belok, Surabaya','087898765432');
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Diki Adita M.','Jl. Mundur Kampung Mundur, Lamongan','087832165498');
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Thanos','Planet Titan','081313424565');
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Mr. Bean','Inggris','081435666777');

INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Isaac A. Lovecraft','Pandemonium Palace','051xxxxxxxxx');


-- patient_detail
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Diki Adita M','1997/11/10','Lamongan','L','Ganggang,Medalem,Modo-Lamongan','Driver Gojek','085706812866');
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('M Aji Pangestu','1997/08/12','Sidoarjo','L','Jl.Rajawali No.6, Sedati-Sidoarjo','Driver Gojek','085612143612');
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Badruz Zaman','1996/06/02','Sidoarjo','L','Jl.Kolonel Sugiono,Panjunan-Sidioaro','Pedagang Ikan','085647586000');
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Park Chaeyong','1997/06/05','Korea','P','Jl.Seoul No.101-Korea','Idol','0825276488774');
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Jihan Audy','1999/01/11','Mojokerto','P','Jl.Jylo Manise-Mojokerto','Penyanyi','085653625421');



-- session
INSERT INTO `session`(`date`, `shift`, `doctor`, `pharmacist`)
	 VALUES ('2019/10/02','Pagi',2,3);
INSERT INTO session (date,date,shift,doctor,pharmacist) 
	VALUES ('2019/10/03','Sore',2,3);

-- medical_record
INSERT INTO `medical_record`(`patient`, `symptoms`, `session`, `type_case`) 
	VALUES (3,'1. Hidup susah 2. Batuk di telinga',1,'Lama');
INSERT INTO `medical_record`(`patient`, `symptoms`, `session`, `type_case`) 
	VALUES (1,'1. Kalau pas merem mata saya gelap 2. Habis makan perut terasa kenyang',1,'Baru');
INSERT INTO `medical_record`(`patient`, `symptoms`, `session`, `type_case`) 
	VALUES (5,'1. Sakit tapi tidak berdarah',1,'Lama');


-- perscription
INSERT INTO `prescription`(`medical_record`, `medicines`, `dose`, `detail`)
	VALUES (2,1,'50 mg','Diminum saat sedang minum 2 x 24 jam');
INSERT INTO `prescription`(`medical_record`, `medicines`, `dose`, `detail`)
	VALUES (2,2,'10 mg','Diminum 1 tablet 1 kali minum');
INSERT INTO `prescription`(`medical_record`, `medicines`, `dose`, `detail`)
	VALUES (3,3,'20 mg','Diminum kalau sedang kumat');


-- account
INSERT INTO account (account_type,username,password,user_detail,class) 
	VALUES (0,'admin_sadis','adminsadis',1,'admin');
INSERT INTO account (account_type,username,password,user_detail,class) 
	VALUES (2,'dokter_cabul','doktercabul',2,'doctor');
INSERT INTO account (account_type,username,password,user_detail,class) 
	VALUES (3,'apotek_gila','apotekgila',3,'pharmacist');
INSERT INTO account (account_type,username,password,user_detail,class) 
	VALUES (1,'ecpp','ecpp',6,'frontdesk');


-- patient
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1001,'1234567890001',1);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1422,'4321567888772',2);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (4433,'3421567876593',3);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1011,'PU',4);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (9912,'PU',5);

-- medical_record
INSERT INTO medical_record (patient,symptoms,session,type_case) 
	VALUES (4,'1. ?? 2. ?? ??',1,'Baru');
INSERT INTO medical_record (patient,symptoms,session,type_case) 
	VALUES (4,'Jantung  Di isi Air Bocor',2,'Baru');
INSERT INTO medical_record (patient,symptoms,session,type_case) 
	VALUES (2,'Darah Biru Karena Minum Tinta Biru',1,'Lama');

