# user_detail

```mysql
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Mohammad Ajiemas P.','Jl. Lurus Kampung Lurus, Sidoarjo','087812345678');
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('M. Taufan Agusta','Jl. Belok Kampung Belok, Surabaya','087898765432');
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Diki Adita M.','Jl. Mundur Kampung Mundur, Lamongan','087832165498');
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Thanos','Planet Titan','081313424565');
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Mr. Bean','Inggris','081435666777');
INSERT INTO `user_detail`(`fullname`, `home_address`, `contact`)
	VALUES ('Isaac A. Lovecraft','Pandemonium Palace','051xxxxxxxxx');
```



# account

```mysql
INSERT INTO account (account_type,username,password,user_detail,class) 
	VALUES (0,'admin_sadis','adminsadis',1,'admin');
INSERT INTO account (account_type,username,password,user_detail,class) 
	VALUES (2,'dokter_cabul','doktercabul',2,'doctor');
INSERT INTO account (account_type,username,password,user_detail,class) 
	VALUES (3,'apotek_gila','apotekgila',3,'pharmacist');
INSERT INTO account (account_type,username,password,user_detail,class) 
	VALUES (1,'ecpp','ecpp',6,'frontdesk');
```



# patient_detail

```mysql
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Diki Adita M','1997/11/10','Lamongan','L','Ganggang,Medalem,Modo-Lamongan','Driver Gojek','085706812866');
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('M Aji Pangestu','1997/08/12','Sidoarjo','L','Jl.Rajawali No.6, Sedati-Sidoarjo','Driver Gojek','085612143612');
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Badruz Zaman','1996/06/02','Sidoarjo','L','Jl.Kolonel Sugiono,Panjunan-Sidioaro','Pedagang Ikan','085647586000');
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Park Chaeyong','1997/06/05','Korea','P','Jl.Seoul No.101-Korea','Idol','0825276488774');
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Jihan Audy','1999/01/11','Mojokerto','P','Jl.Jylo Manise-Mojokerto','Penyanyi','085653625421');
	
	--
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Bejo Sudarmono','1979/01/19','Mojokerto','L','Jl.Brawijaya-Mojokerto','Petani','0856578642361');	
INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Aji Santoso','1983/07/6','Lamongan','L','JlGotongroyong, Lamongrejo -  Lamongan','Pelatih Sepakbola','081764273542');	
	INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Deddyb Kurniawan','1997/08/10','Surabaya','L','Jl.Simo Kalasan No.10 - Surabaya','PT.Garudafood Indonesia','08562634389');
	INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Sundayati','1982/03/17','Surabaya','P','Jl. Lidah Wetan No.10 - Surabaya','PT. Telkom Indonesia','08122334556');
	INSERT INTO patient_detail (fullname,date_ofbirth,place_ofbirth,sex,home_address,firm,contact) 
	VALUES ('Suminah','1975/03/10','Surabaya','P','Jl. Lidah Timur No.10 - Surabaya','PT. Indosat Oredo','08123437666');
--

```



# patient

```mysql
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1001,'1234567890001',1);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1422,'4321567888772',2);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (4433,'3421567876593',3);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1011,'PU',4);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (9912,'PU',5);
	
	--
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1007,'1234546890005',6);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1725,'4323667644772',7);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1122,'3421567871122',8);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (1033,'PU',9);
INSERT INTO patient (id_index,id_bpjs,patient_detail) 
	VALUES (4412,'PU',10);
```



# medicine

```mysql
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Amoxicillin Trihydrate','Antibiotik','Kaplet','500 mg',100,'Strip');
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Piroxicam','Analgetik','Tablet','20 mg',100,'Strip');
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Paracetamol','Antibiotik','Tablet','500 mg',100,'Strip');
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Novagesic Paracetamol','Antibiotik','Sirup','60 ml',100,'Botol');
INSERT INTO `medicine`(`name`, `type`, `shape`, `dose`, `stock`, `unit`)
	VALUES ('Antasida Doen','Gastrointestinal','Sirup','60 ml',100,'Botol');
```





# queue





# session

```mysql
INSERT INTO `session`(`date`, `shift`, `doctor`, `pharmacist`)
	 VALUES ('2019/10/10','Pagi',2,3);
```

