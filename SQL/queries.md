# queries



## patient



Menampilkan semua row tabel patien, patien_detail dan medical_record

```mysql
SELECT * FROM patient, patient_detail, medical_record
 WHERE medical_record.patient = patient.id ANd patient.patient_detail = patient_detail.id;
```



patient + detail

```mysql
SELECT * FROM patient, patient_detail
 WHERE patient.patient_detail = patient_detail.id;
```





patient + detail (ID, FULLNAME, INDEX, BPJS)

```mysql
SELECT patient.id, patient_detail.fullname, patient.id_index, patient.id_bpjs FROM patient, patient_detail
 WHERE patient.patient_detail = patient_detail.id;
```



patient + detail (ID, FULLNAME, INDEX, BPJS)

```mysql
SELECT patient.id, patient_detail.fullname, patient.id_index, patient.id_bpjs FROM patient, patient_detail
 WHERE patient_detail.fullname = '%aji%' AND patient.patient_detail = patient_detail.id;
```







Menampilkan id bpjs, detail pasien beserta riwayat penyakit

```mysql
SELECT patient.id_bpjs, patient_detail.fullname, medical_record.symptoms
 FROM patient, patient_detail, medical_record
 WHERE medical_record.patient = patient.id ANd patient.patient_detail = patient_detail.id;
```







## INNER JOIN





Menampilkanaccount dan user detail

```mysql
SELECT * FROM account INNER JOIN user_detail ON account.user_detail = user_detail.id;
```



Menampilkan id bpj, nama, dan penyakit

```mysql
SELECT patient.id_bpjs, patient_detail.fullname, medical_record.symptoms
FROM patient
    INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id
    INNER JOIN medical_record ON medical_record.patient  = patient.id;
```



ID, FULLNAME, INDEX, BPJS, using LIKE

```mysql
SELECT patient.id, patient_detail.fullname, patient.id_index, patient.id_bpjs
FROM patient
    INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id WHERE patient_detail.fullname LIKE '%Aji%'
```



PATIENT * using LIKE

```mysql
SELECT * FROM patient
    INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id WHERE CAST (patient.id_index as TEXT) LIKE '%20%'
```









Menampilkan id bpjs, nama, penyakit, dan obat

```mysql
SELECT patient.id_bpjs, patient_detail.fullname, medical_record.symptoms, medicine.name 
FROM patient 
	INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id
	INNER JOIN medical_record ON medical_record.patient = patient.id
	INNER JOIN prescription ON medical_record.id = prescription.medical_record
	INNER JOIN medicine ON medicine.id = prescription.medicines;
```



```mysql
SELECT patient.id_bpjs, patient_detail.fullname, patient_detail.date_ofbirth, medical_record.symptoms, medicine.name, medicine.dose as medicine_dose, prescription.dose as prescription_dose
FROM patient 
	INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id
	INNER JOIN medical_record ON medical_record.patient = patient.id
	INNER JOIN prescription ON medical_record.id = prescription.medical_record
	INNER JOIN medicine ON medicine.id = prescription.medicines;
```



Menampilkan penyakit, dan obat

```mysql
SELECT medical_record.symptoms, medicine.name
FROM medicine
    INNER JOIN prescription ON medicine.id = prescription.medicines
    INNER JOIN medical_record ON prescription.medical_record = medical_record.id;
```





## LEFT JOIN



Menampilkan semua row di tabel account dan semua row tabel user_detail yang berhubungan dengan tabel account

```mysql
SELECT * FROM account LEFT JOIN user_detail ON account.user_detail = user_detail.id;
```


# queries



## patient



Menampilkan semua row tabel patien, patien_detail dan medical_record

```mysql
SELECT * FROM patient, patient_detail, medical_record
 WHERE medical_record.patient = patient.id ANd patient.patient_detail = patient_detail.id;
```



Menampilkan id bpjs, detail pasien beserta riwayat penyakit

```mysql
SELECT patient.id_bpjs, patient_detail.fullname, medical_record.symptoms
 FROM patient, patient_detail, medical_record
 WHERE medical_record.patient = patient.id ANd patient.patient_detail = patient_detail.id;
```







## INNER JOIN





Menampilkanaccount dan user detail

```mysql
SELECT * FROM account INNER JOIN user_detail ON account.user_detail = user_detail.id;
```



Menampilkan id bpj, nama, dan penyakit

```mysql
SELECT patient.id_bpjs, patient_detail.fullname, medical_record.symptoms
FROM patient
    INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id
    INNER JOIN medical_record ON medical_record.patient  = patient.id;
```



Menampilkan id bpjs, nama, penyakit, dan obat

```mysql
SELECT patient.id_bpjs, patient_detail.fullname, medical_record.symptoms, medicine.name 
FROM patient 
	INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id
	INNER JOIN medical_record ON medical_record.patient = patient.id
	INNER JOIN prescription ON medical_record.id = prescription.medical_record
	INNER JOIN medicine ON medicine.id = prescription.medicines;
```



```mysql
SELECT patient.id_bpjs, patient_detail.fullname, patient_detail.date_ofbirth, medical_record.symptoms, medicine.name, medicine.dose as medicine_dose, prescription.dose as prescription_dose
FROM patient 
	INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id
	INNER JOIN medical_record ON medical_record.patient = patient.id
	INNER JOIN prescription ON medical_record.id = prescription.medical_record
	INNER JOIN medicine ON medicine.id = prescription.medicines;
```



Menampilkan penyakit, dan obat

```mysql
SELECT medical_record.symptoms, medicine.name
FROM medicine
    INNER JOIN prescription ON medicine.id = prescription.medicines
    INNER JOIN medical_record ON prescription.medical_record = medical_record.id;
```





## LEFT JOIN



Menampilkan semua row di tabel account dan semua row tabel user_detail yang berhubungan dengan tabel account

```mysql
SELECT * FROM account LEFT JOIN user_detail ON account.user_detail = user_detail.id;
```

