CREATE TABLE `medicine` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(64) NOT NULL,
  `type` VARCHAR(32) NOT NULL,
  `shape` VARCHAR(8) NOT NULL,
  `dose` VARCHAR(8) NOT NULL,
  `stock` SMALLINT,
  `unit` VARCHAR(8) NOT NULL
);

CREATE TABLE `patient_detail` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `fullname` VARCHAR(64) NOT NULL,
  `date_ofbirth` DATE,
  `place_ofbirth` VARCHAR(16) NOT NULL,
  `sex` VARCHAR(1) NOT NULL,
  `home_address` VARCHAR(64) NOT NULL,
  `firm` VARCHAR(32),
  `contact` VARCHAR(13) NOT NULL
);

CREATE TABLE `patient` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `id_index` INTEGER,
  `id_bpjs` VARCHAR(16) NOT NULL,
  `patient_detail` INTEGER NOT NULL
);

CREATE INDEX `idx_patient__patient_detail` ON `patient` (`patient_detail`);

ALTER TABLE `patient` ADD CONSTRAINT `fk_patient__patient_detail` FOREIGN KEY (`patient_detail`) REFERENCES `patient_detail` (`id`);

CREATE TABLE `medical_detail` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `detail` VARCHAR(32) NOT NULL,
  `patient` INTEGER
);

CREATE INDEX `idx_medical_detail__patient` ON `medical_detail` (`patient`);

ALTER TABLE `medical_detail` ADD CONSTRAINT `fk_medical_detail__patient` FOREIGN KEY (`patient`) REFERENCES `patient` (`id`) ON DELETE SET NULL;

CREATE TABLE `user_detail` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `fullname` VARCHAR(64) NOT NULL,
  `home_address` VARCHAR(128) NOT NULL,
  `contact` VARCHAR(16) NOT NULL
);

CREATE TABLE `account` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `account_type` TINYINT,
  `username` VARCHAR(16) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  `last_log` DATETIME,
  `user_detail` INTEGER NOT NULL,
  `classtype` VARCHAR(255) NOT NULL
);

CREATE INDEX `idx_account__user_detail` ON `account` (`user_detail`);

ALTER TABLE `account` ADD CONSTRAINT `fk_account__user_detail` FOREIGN KEY (`user_detail`) REFERENCES `user_detail` (`id`);

CREATE TABLE `session` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `date` DATE,
  `shift` VARCHAR(8) NOT NULL,
  `doctor` INTEGER NOT NULL,
  `pharmacist` INTEGER NOT NULL
);

CREATE INDEX `idx_session__doctor` ON `session` (`doctor`);

CREATE INDEX `idx_session__pharmacist` ON `session` (`pharmacist`);

ALTER TABLE `session` ADD CONSTRAINT `fk_session__doctor` FOREIGN KEY (`doctor`) REFERENCES `account` (`id`);

ALTER TABLE `session` ADD CONSTRAINT `fk_session__pharmacist` FOREIGN KEY (`pharmacist`) REFERENCES `account` (`id`);

CREATE TABLE `medical_record` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `patient` INTEGER NOT NULL,
  `symptoms` VARCHAR(512) NOT NULL,
  `bp_systolic` SMALLINT,
  `bp_diastolic` SMALLINT,
  `session` INTEGER NOT NULL,
  `type_case` VARCHAR(4) NOT NULL
);

CREATE INDEX `idx_medical_record__patient` ON `medical_record` (`patient`);

CREATE INDEX `idx_medical_record__session` ON `medical_record` (`session`);

ALTER TABLE `medical_record` ADD CONSTRAINT `fk_medical_record__patient` FOREIGN KEY (`patient`) REFERENCES `patient` (`id`) ON DELETE CASCADE;

ALTER TABLE `medical_record` ADD CONSTRAINT `fk_medical_record__session` FOREIGN KEY (`session`) REFERENCES `session` (`id`) ON DELETE CASCADE;

CREATE TABLE `prescription` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `medical_record` INTEGER NOT NULL,
  `medicines` INTEGER NOT NULL,
  `dosage` VARCHAR(8) NOT NULL,
  `detail` VARCHAR(256) NOT NULL
);

CREATE INDEX `idx_prescription__medical_record` ON `prescription` (`medical_record`);

CREATE INDEX `idx_prescription__medicines` ON `prescription` (`medicines`);

ALTER TABLE `prescription` ADD CONSTRAINT `fk_prescription__medical_record` FOREIGN KEY (`medical_record`) REFERENCES `medical_record` (`id`) ON DELETE CASCADE;

ALTER TABLE `prescription` ADD CONSTRAINT `fk_prescription__medicines` FOREIGN KEY (`medicines`) REFERENCES `medicine` (`id`) ON DELETE CASCADE;

CREATE TABLE `queue` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `patient` INTEGER NOT NULL,
  `queue_position` TINYINT,
  `timestamp` TIME,
  `session` INTEGER NOT NULL
);

CREATE INDEX `idx_queue__patient` ON `queue` (`patient`);

CREATE INDEX `idx_queue__session` ON `queue` (`session`);

ALTER TABLE `queue` ADD CONSTRAINT `fk_queue__patient` FOREIGN KEY (`patient`) REFERENCES `patient` (`id`);

ALTER TABLE `queue` ADD CONSTRAINT `fk_queue__session` FOREIGN KEY (`session`) REFERENCES `session` (`id`) ON DELETE CASCADE