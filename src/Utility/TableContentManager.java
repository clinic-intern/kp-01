package Utility;
import Patient.Patient;
import Patient.Queue;
import Utility.DatabaseAccessor;
import Utility.DatabaseConnector;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;


public class TableContentManager {
    
    public void QPopulate(DefaultTableModel sourceTable){
        
        DefaultTableModel model = sourceTable;
        
        ArrayList<Patient> patients = DatabaseAccessor.GetPatient();
        
        String[] column = {"No.", "Nama Pasien", "No. BPJS"};
        String[][] data = { {"1", "Lorem Ipsum", "0000000000000"},
                            {"1", patients.get(0).Get_fullname(), patients.get(0).Get_bpjs_id()}
        
                            };
        
        System.out.println(patients.get(1).Get_date_ofbirth());
        model.setDataVector(data, column);
        
    }
    
    public void QueueMonitor(DefaultTableModel sourceTable){
        
        DefaultTableModel model = sourceTable;
        ArrayList<Patient> patients = DatabaseAccessor.GetPatient();
        
        // GET PATIENT WHICH ENQUEUE
        // CHECK DB, EXIST ? ADD : CREATE, ADD
        // INSERT -> QUEUE----> SELCT TO TABLE
        
        String[] column = {"No.", "Nama"," ", "Perserta BPJS"};
        String[][] data = { {"1", "Mr. Wick", "$14 MILLION" ,"Excaommunicado"},
                            {Integer.toString(patients.get(0).Get_id()), patients.get(0).Get_fullname(), patients.get(0).Get_local_id(), patients.get(0).Get_bpjs_id()}, 
                            {Integer.toString(patients.get(1).Get_id()), patients.get(1).Get_fullname(), patients.get(1).Get_local_id(), patients.get(1).Get_bpjs_id()}

                            };
        
//        System.out.println(patients.get(1).Get_date_ofbirth());
        model.setDataVector(data, column);

        
    }
   
    public void TableContentCreator(String[] column, DefaultTableModel sourceTable){
        DefaultTableModel model = sourceTable;
        
        String[][] data = {{}};
        
        model.setDataVector(data, column);

    }
    
    public void Populate_PatientSearch(String[] columnName, DefaultTableModel sourceTable, 
            String patientName, String patientIndex){
        
        DefaultTableModel model = sourceTable;
        System.out.println("TableContentManager.TableContentCreator_FRONTDESK: CALLED");
        
        ArrayList<Patient> patients = DatabaseAccessor.SearchPatient(patientName, patientIndex);
        int totalRow = patients.size();
//        int totalColumn = Patient.totalColumn;
        int totalColumn = columnName.length;
        
        String[][] tableReady = new String[totalRow][totalColumn];
       
        int resultOrder = 1;
        
        for(int row = 0; row < totalRow; row++){
           
           tableReady[row][0] = Integer.toString(resultOrder);
           tableReady[row][1] = patients.get(row).Get_fullname();
           tableReady[row][2] = patients.get(row).Get_local_id();
           tableReady[row][3] = patients.get(row).Get_bpjs_id();
           
           resultOrder++;
        }
        model.setDataVector(tableReady, columnName);
    }
    
        public void Populate_PatientSearch_ID_HIDDEN(String[] columnName, DefaultTableModel sourceTable, 
            String patientName, String patientIndex){
        
        DefaultTableModel model = sourceTable;
        System.out.println("TableContentManager.TableContentCreator_FRONTDESK: CALLED");
        
        Queue.Patients_STATIC = DatabaseAccessor.SearchPatient(patientName, patientIndex);
        int totalRow = Queue.Patients_STATIC.size();
//        int totalColumn = Patient.totalColumn;
        int totalColumn = columnName.length + 1;
        
        String[][] tableReady = new String[totalRow][totalColumn];
       
        int resultOrder = 1;
        
        for(int row = 0; row < totalRow; row++){
           
           tableReady[row][0] = Integer.toString(resultOrder);
           tableReady[row][1] = Queue.Patients_STATIC.get(row).Get_fullname();
           tableReady[row][2] = Queue.Patients_STATIC.get(row).Get_local_id();
           tableReady[row][3] = Queue.Patients_STATIC.get(row).Get_bpjs_id();
           tableReady[row][4] = Integer.toString(Queue.Patients_STATIC.get(row).Get_id());
           
           System.out.println(tableReady[row][4]);
           
           resultOrder++;
        }
        model.setDataVector(tableReady, columnName);
    }
    
    public void Populate_PatientQueue(){
    
    }
    
}
