package Utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseConnector {
    
    private final static String URL = "jdbc:mysql://localhost/kp_01";
    private final static String DRIVER = "com.mysql.jdbc.Driver";
    private final static String USERNAME= "root";
    private final static String PASSWORD= "";
    
    public static Connection connection = null;
    
    static {
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            
        } catch (ClassNotFoundException | SQLException e) { }
        
    }
    
    public static void ConnectionCheck(){
        
        String open = "Database connection success";
        String closed = "Data connection failed";

        try {
            if(connection == null)
                System.out.println(closed);
            
            else{
                boolean isClosed = connection.isClosed();
                String databaseStatus = isClosed ? closed : open;
            
                System.out.println(databaseStatus);
            
            }

        } catch (SQLException e) { }
        
    }
    
    
}
