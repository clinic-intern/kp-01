package Utility;

public class QueryBuilder {
    
    public static String PatientSearchBy(String patientName){

        String like_name = "'%" + patientName + "%'";
        
        String query = "SELECT * FROM patient " +
            "INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id " + 
            "WHERE patient_detail.fullname LIKE " + like_name;

        return query;
    }
    
        public static String PatientSearchBy(String patientName, String patientLocalIndex){
            
            String query = "SELECT * FROM patient " +
                "INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id ";
            
            String like_name = "'%" + patientName + "%'";
            String index = "AND patient.id_index = " + patientLocalIndex;
  
            String condition = "";
        
            // search by name
            if(!patientName.equals("")){
                condition = "WHERE patient_detail.fullname LIKE " + like_name; 
            
                if(!patientLocalIndex.equals("")){
                    condition += index;
                }
            
            // search by index    
            }else if(patientName.equals("") && !patientLocalIndex.equals("")){
                condition = "WHERE patient.id_index = " + patientLocalIndex;
            }
        
            query += condition;
            DEBUG_QUERY(query);
        
            return query;
    }
    
        public static void DEBUG_QUERY(String query){
            System.out.println(query);
        }
    
}
