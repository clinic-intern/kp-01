package Utility;

public class LoginCache {
    
    private String username;
    private int userType;
    private boolean logInVerfied;
    
    public LoginCache(){
        this.username = null;
        this.userType = -1;
        this.logInVerfied = false;
    }
    
    
    public LoginCache(String username, int userType, boolean logInVerfied){
        this.username = username;
        this.userType = userType;
        this.logInVerfied = logInVerfied;
    }
    
    
   public String Get_User(){
       return username;
   }
   
   public int Get_UserType(){
       return userType;
   }
   
   public boolean Get_LogInVerifed(){
       return logInVerfied;
   }
    
}
