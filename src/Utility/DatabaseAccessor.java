package Utility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Patient.Patient;


public class DatabaseAccessor {
    
    public static Statement statement_obj = null;
    public static PreparedStatement preparedStatement_obj = null;
    public static ResultSet resultSet_obj = null;
    
    public static String Get_Username(String username){
        
        String result = null;
        String query = "SELECT username FROM account WHERE username = ?";
        
        try {
            Fetching(query, username);
            result = resultSet_obj.getString(1);
        } catch (SQLException e) { }
        finally {
            Close();
        }

        return result; 
    }
    
    public static String Get_Password(String password){
        
        String result = null;
        String query = "SELECT password FROM account WHERE password = ?";
        
        try {
            Fetching(query, password);
            result = resultSet_obj.getString(1);
        } catch (SQLException e) { }
        finally {
            Close();
        }
        
        return result; 
    }
    
    public static int Get_UserType(String username){
        
        int result = -1;
        String query = "SELECT account_type FROM account WHERE username = ?";
        
        try {
            Fetching(query, username);
            result = resultSet_obj.getInt(1);
        } catch (SQLException e) { }
        finally {
            Close();
        }
                
        return result;
                
    }  
        
    public static ArrayList GetPatient(){
    
       String query = "SELECT * FROM patient, patient_detail "
               + "WHERE patient.patient_detail = patient_detail.id";
       ArrayList<Patient> patients = new ArrayList<Patient>();
       
        int count = 0;
        
       try {
//          Fetching(query);
            PreparedStatement ps = DatabaseConnector.connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery(query);
            
            while(rs.next()){
                count++;
//                System.out.println(rs.getString("patient.id_bpjs"));
                patients.add(new Patient(
                        rs.getInt("patient.id"), 
                        rs.getString("patient.id_index"),
                        rs.getString("patient.id_bpjs"), 
                        rs.getString("patient_detail.fullname"),
                        rs.getDate("patient_detail.date_ofbirth"),
                        rs.getString("patient_detail.place_ofbirth"),
                        rs.getString("patient_detail.sex").charAt(0),
                        rs.getString("home_address"),
                        rs.getString("firm"),
                        rs.getString("contact")
                
                )
                );
            }

        } catch (SQLException e) { }
        finally {
            Close();
        }
       
       return patients;
        
    }
    
    
    public static ArrayList SearchPatient(String patientName, String patientIndex)
    {
        System.out.println("DatabaseAccesor.SearchPatient: CALLED");

        ArrayList<Patient> patients = new ArrayList<Patient>();
        String query = QueryBuilder.PatientSearchBy(patientName, patientIndex);
        
        int count = 0;

        try {
            
            PreparedStatement prepStatement = DatabaseConnector.connection.prepareStatement(query);
            ResultSet rs = prepStatement.executeQuery(query);
            
            while(rs.next()){
                count++;
                patients.add(new Patient(
                        rs.getInt("patient.id"), 
                        rs.getString("patient.id_index"),
                        rs.getString("patient.id_bpjs"), 
                        rs.getString("patient_detail.fullname"),
                        rs.getDate("patient_detail.date_ofbirth"),
                        rs.getString("patient_detail.place_ofbirth"),
                        rs.getString("patient_detail.sex").charAt(0),
                        rs.getString("home_address"),
                        rs.getString("firm"),
                        rs.getString("contact")
                )
                );
            }

        } catch (SQLException e) { }
    
            System.out.println("Patient Search count: " + count);
//        System.out.println(patients.get(0).Get_fullname());
        
        return patients;
        
    }
    
    public static void SearchPatient_DEBUG(String name)
    {
        Connection connection = DatabaseConnector.connection;
        ArrayList<Patient> patients = new ArrayList<Patient>();
        int count = 0;
        
        String query = "SELECT * FROM patient " +
            "INNER JOIN patient_detail ON patient.patient_detail = patient_detail.id " + 
             "WHERE patient_detail.fullname LIKE '%?%'";
   
        try {
             PreparedStatement ps = DatabaseConnector.connection.prepareStatement(query);
             ps.setString(1, name);
             ResultSet rs = ps.executeQuery();
//             resultSet.next();
            
            while(rs.next()){
                count++;
                System.out.println(rs.getString("patient.id_bpjs"));
//                patients.add(new Patient(
//                        resultSet.getInt("patient.id"), 
//                        resultSet.getString("patient.id_index"),
//                        resultSet.getString("patient.id_bpjs"), 
//                        resultSet.getString("patient_detail.fullname"),
//                        resultSet.getDate("patient_detail.date_ofbirth"),
//                        resultSet.getString("patient_detail.place_ofbirth"),
//                        resultSet.getString("patient_detail.sex").charAt(0),
//                        resultSet.getString("home_address"),
//                        resultSet.getString("firm"),
//                        resultSet.getString("contact"))
//                );
            }

        } catch (SQLException e) { }
    
            System.out.println(count);
//        System.out.println(patients.get(0).Get_fullname());
        
//        return patients;
        
    }
    
    
    private static void Fetching(String query){
        
        Connection connection = DatabaseConnector.connection;
        
        try {
             preparedStatement_obj = connection.prepareStatement(query);
             resultSet_obj = preparedStatement_obj.executeQuery();
             //resultSet.next();

        } catch (SQLException e) { }
        
    }
    
    private static void Fetching(String query, String first){
        
        Connection connection = DatabaseConnector.connection;
        
        try {
             preparedStatement_obj = connection.prepareStatement(query);
             preparedStatement_obj.setString(1, first);
             resultSet_obj = preparedStatement_obj.executeQuery();
             resultSet_obj.next();

        } catch (SQLException e) { }
        
    }
    
    public static void Close(){
            try {
                if(resultSet_obj != null)
                resultSet_obj.close();
            } catch (SQLException e) { }
            
            try {
                if(statement_obj != null)
                statement_obj.close();
            } catch (SQLException e) { }
            
            try {
                if(preparedStatement_obj != null)
                preparedStatement_obj.close();
            } catch (SQLException e) { }

    }
    
    
}
