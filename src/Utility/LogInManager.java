package Utility;

public  class LogInManager 
{
    public static boolean LogInVerification(String username, String password){
    
        String local_username = null;
        String local_password = null;
        int account_type = -1;
        boolean verification = false;

        
        try {
            local_username = DatabaseAccessor.Get_Username(username);
            local_password = DatabaseAccessor.Get_Password(password);
            account_type = DatabaseAccessor.Get_UserType(username);

            
            verification = local_username.equals(username) && 
                    local_password.equals(password);
            
        } catch (Exception e) { }
        

        //System.out.println(local_username);
        //System.out.println(account_type);
        //System.out.println(verification);
        
        return verification;
    }
    
    
    public static LoginCache LogPass(String username, String password){
    
        LoginCache loginCache = new LoginCache();
        String local_username = null;
        String local_password = null;
        int account_type = -1;
        boolean verification = false;
        

        
        try {
            local_username = DatabaseAccessor.Get_Username(username);
            local_password = DatabaseAccessor.Get_Password(password);
            account_type = DatabaseAccessor.Get_UserType(username);

            verification = local_username.equals(username) && 
                    local_password.equals(password);
            
            loginCache = new LoginCache(username, account_type, verification);
            
        } catch (Exception e) { }
        

        //System.out.println(local_username);
        //System.out.println(account_type);
        //System.out.println(verification);
        
        return loginCache;
    }
    
}
