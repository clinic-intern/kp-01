package kp.pkg01;

import UserInterface.Behaviour.PanelManager;
import Utility.DatabaseConnector;
import Utility.LoginCache;

public class Initiate {
    
    public static PanelManager panelManager = new PanelManager();
//    public static LoginCache loginCache = new LoginCache();
    
    public static void main(String[] args){
        
        DatabaseConnector.ConnectionCheck();
        
            java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                panelManager.ActivatePanel();
//                
            }
        });
            

        
        
    }
    
    
}
