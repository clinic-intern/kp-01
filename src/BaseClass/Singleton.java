package BaseClass;

public class Singleton{
    
    protected static Singleton instance = null;
    
    protected Singleton(){ }
    
    protected static Singleton getInstance(){
        if(instance == null)
            instance = new Singleton();
    
        return instance;
    }
    
    
}
