package UserInterface.Panel;

import Utility.TableContentManager;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

public class FrontdeskPanel extends javax.swing.JFrame {
    
//    public QueuePanel QPanel;
//    static javax.swing.JFrame QueueMonitor = new QueuePanel();

    TableContentManager contentManager = new TableContentManager();
    
    private String[] patientSearchColumn = {"No", "Nama", "No. Kartu", "No. Peserta BPJS"}; 
    
    public FrontdeskPanel() {
        initComponents();
        PanelBehaviour();
        
//        QPanel = new QueuePanel();
//        contentManager.QPopulate((DefaultTableModel) QueueTable.getModel());
//        contentManager.QueueMonitor((DefaultTableModel) QPanel.Get_QueueMonitorPanel().getModel() ) ;

        // patient search
        contentManager.TableContentCreator(patientSearchColumn, (DefaultTableModel) patientSearch_Table.getModel());
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        patientSearch_Table = new javax.swing.JTable();
        patientName_Label = new javax.swing.JLabel();
        patientLocalIndex_Label = new javax.swing.JLabel();
        bpjsId_Label = new javax.swing.JLabel();
        patientName_TextBox = new javax.swing.JTextField();
        IndexPatient_TextBox = new javax.swing.JTextField();
        bpjsId_TextBox = new javax.swing.JTextField();
        patient_dob_Label = new javax.swing.JLabel();
        dob_monthComboBox = new javax.swing.JComboBox<>();
        dob_date_TextBox = new javax.swing.JTextField();
        dob_year_TextBox = new javax.swing.JTextField();
        searchPatient_Button = new javax.swing.JButton();
        enqueuePatient_Button = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        QueueTable = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        patientSearch_Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(patientSearch_Table);

        patientName_Label.setText("Nama");

        patientLocalIndex_Label.setText("Index");

        bpjsId_Label.setText("No. BPJS");

        patient_dob_Label.setText("Tanggal Lahir");

        dob_monthComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" }));

        searchPatient_Button.setText("Tampilkan");

        enqueuePatient_Button.setText("Masukan Antrian");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 548, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(patientName_Label)
                                .addComponent(patientLocalIndex_Label)
                                .addComponent(bpjsId_Label))
                            .addGap(40, 40, 40)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(patientName_TextBox)
                                .addComponent(IndexPatient_TextBox)
                                .addComponent(bpjsId_TextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(patient_dob_Label)
                            .addGap(18, 18, 18)
                            .addComponent(dob_date_TextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(dob_monthComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(dob_year_TextBox)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(searchPatient_Button)
                        .addGap(204, 204, 204)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(enqueuePatient_Button)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(patientName_Label)
                    .addComponent(patientName_TextBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(enqueuePatient_Button))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(patientLocalIndex_Label)
                    .addComponent(IndexPatient_TextBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bpjsId_Label)
                    .addComponent(bpjsId_TextBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(patient_dob_Label)
                    .addComponent(dob_monthComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dob_date_TextBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dob_year_TextBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(searchPatient_Button)
                .addGap(0, 52, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Utama", jPanel1);

        QueueTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(QueueTable);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 528, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 307, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Antrian", jPanel2);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 548, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 329, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Daftar Pasien", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void PanelBehaviour(){
//        this.setLocationRelativeTo(null);
//        this.setExtendedState(javax.swing.JFrame.MAXIMIZED_BOTH); 
//        this.setUndecorated(true);
    
    }
    
    public String[] Get_PatientSearchColumn(){
        return patientSearchColumn;
    }
    
    public String Get_PatientName(){
        return patientName_TextBox.getText();
    }
    
    public String Get_IndexPatient(){
        String index = IndexPatient_TextBox.getText();
        return index == null ? "0" : index; 
    }
    
    public String Get_BPJS_Id(){
        return bpjsId_TextBox.getText();
    }
    
    public String Get_dob_date(){
        return dob_date_TextBox.getText();
    }
    
    public int Get_dob_month(){
        return dob_monthComboBox.getSelectedIndex();
    }
    
    public String dob_year() {
        return dob_year_TextBox.getText();
    }  
    
    public DefaultTableModel Get_PatientSearchTableModel(){
        return (DefaultTableModel) patientSearch_Table.getModel();
    }
    
    public javax.swing.JTable Get_PatientSearchTable(){
        return patientSearch_Table;
    }
//    public DefaultTableModel Get
            
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField IndexPatient_TextBox;
    private javax.swing.JTable QueueTable;
    private javax.swing.JLabel bpjsId_Label;
    private javax.swing.JTextField bpjsId_TextBox;
    private javax.swing.JTextField dob_date_TextBox;
    private javax.swing.JComboBox<String> dob_monthComboBox;
    private javax.swing.JTextField dob_year_TextBox;
    public javax.swing.JButton enqueuePatient_Button;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel patientLocalIndex_Label;
    private javax.swing.JLabel patientName_Label;
    private javax.swing.JTextField patientName_TextBox;
    private javax.swing.JTable patientSearch_Table;
    private javax.swing.JLabel patient_dob_Label;
    public javax.swing.JButton searchPatient_Button;
    // End of variables declaration//GEN-END:variables
}
