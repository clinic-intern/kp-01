package UserInterface.Behaviour;

import Patient.Queue;
import UserInterface.Panel.FrontdeskPanel;
import Utility.TableContentManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.table.DefaultTableModel;

public class FrontdeskBehaviour implements IBehaviour{
    
    private FrontdeskPanel frontdeskPanel;
    private final int HIDDEN_ID_INDEX = 0;
    
    public FrontdeskBehaviour(FrontdeskPanel frontdeskPanel){
        this.frontdeskPanel = frontdeskPanel;
    }
    
    public void Init_Behaviour(){
        
        frontdeskPanel.setVisible(true);
        
        frontdeskPanel.searchPatient_Button.setActionCommand("Search Patient");
        frontdeskPanel.searchPatient_Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(frontdeskPanel.Get_PatientName() != null){
                    
                    String SearchName = frontdeskPanel.Get_PatientName();
                    String SearchIndex = frontdeskPanel.Get_IndexPatient();
                    String SearchBPJS_Id = frontdeskPanel.Get_BPJS_Id();
                    
                    if(SearchIndex.equals(""))
                        System.out.println("Index is NULL");
                    
                    new TableContentManager().Populate_PatientSearch_ID_HIDDEN(
                            frontdeskPanel.Get_PatientSearchColumn(),
                            frontdeskPanel.Get_PatientSearchTableModel(), 
                            SearchName, SearchIndex);
                }
            }
        });
        
        frontdeskPanel.Get_PatientSearchTable().addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DefaultTableModel model = frontdeskPanel.Get_PatientSearchTableModel();
                int selectedRowIndex = frontdeskPanel.Get_PatientSearchTable().getSelectedRow();
                System.out.println("Patient Selected: " + model.getValueAt(selectedRowIndex, HIDDEN_ID_INDEX));
                
                int index = Integer.parseInt(model.getValueAt(selectedRowIndex, HIDDEN_ID_INDEX).toString());
                Queue.Patients_Queue_STATIC.add(Queue.Get_SearchedPatient(index));
                System.out.println("Patient Selected: " + 
                        Queue.Patients_Queue_STATIC.get(0).Get_fullname() + ", ID: " + 
                        Queue.Patients_Queue_STATIC.get(0).Get_id());
                
            }
        });
        
    }
    
    
    public void EnqueueSelectedPatient(){
    
    }
    
}
