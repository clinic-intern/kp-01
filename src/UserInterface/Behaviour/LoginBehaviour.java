package UserInterface.Behaviour;

import UserInterface.Panel.LoginPanel;
import Utility.LoginCache;
import UserInterface.Panel.LoginPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class LoginBehaviour implements IBehaviour{
    
    private LoginPanel loginPanel;
    private PanelManager panelManager;
    
    public LoginBehaviour(LoginPanel loginPanel, PanelManager panelManager){
        this.loginPanel = loginPanel;
        this.panelManager = panelManager;
    }
    
    @Override
    public void Init_Behaviour(){
        
        loginPanel.setVisible(true);
        LogInByEnter();
        
        //Login Button
        loginPanel.button_LogIn.setActionCommand("LogIn");
        loginPanel.button_LogIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                LogInProcedure();
            }
        });
        
        // Exit Button
        loginPanel.button_Exit.setActionCommand("Exit");
        loginPanel.button_Exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });  
    }

    public void LogInProcedure()
    {
        String username = loginPanel.getUsername();
        String password = loginPanel.getPassword();
        
        if(username != null && password != null){
            Utility.LogInManager.LogInVerification(username, password);
            LoginCache loginCache = Utility.LogInManager.LogPass(username, password);
            loginCache = Utility.LogInManager.LogPass(username, password);
                    
            if(loginCache.Get_LogInVerifed()){
                panelManager.ActivatePanel(loginCache.Get_UserType());
                loginPanel.setVisible(false);
            }
            
        }
  
    }
    
    
    public void LogInByEnter() {
                
        loginPanel.getPasswordField().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) { }

            @Override
            public void keyPressed(KeyEvent ke) { 
                if(ke.getKeyCode() == KeyEvent.VK_ENTER){
                    LogInProcedure();
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) { }
        });
    }
    
}
