package UserInterface.Behaviour;

import UserInterface.Behaviour.FrontdeskBehaviour;
import UserInterface.Behaviour.IBehaviour;
import UserInterface.Behaviour.LoginBehaviour;
import UserInterface.Panel.AdminPanel;
import UserInterface.Panel.DoctorPanel;
import UserInterface.Panel.FrontdeskPanel;
import UserInterface.Panel.LoginPanel;
import UserInterface.Panel.PharmacistPanel;
import Utility.DatabaseAccessor;
import Utility.LoginCache;
import Utility.TableContentManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;


public final class PanelManager {
    
    public static LoginPanel loginPanel = new LoginPanel();
    public static JFrame activePanel = null;
//    public static LoginCache loginCache = new LoginCache();
    
    public IBehaviour loginPanel_IB = new LoginBehaviour(new LoginPanel(), this);
    public IBehaviour frontdeskPanel_IB = new FrontdeskBehaviour(new FrontdeskPanel());
    
    // Login Panel Activation
    public void ActivatePanel(){ 
        loginPanel_IB.Init_Behaviour();  
    }
    // User-Specified Panel Activation
    public void ActivatePanel(int type){
        switch(type){
            case 0:
                AdminPanelActivation();
                break;
            case 1:
                FrontdeskPanelActivation();
                break;
            case 2:
                DoctorPanelActivation();
                break;
            case 3:
                PharmacistPanelActivation();
                break;
            default:
                break;
           
        }
    }
    
    
    
    public void AdminPanelActivation(){
        activePanel = new AdminPanel();
        loginPanel.setVisible(false);
        activePanel.setVisible(true);
        activePanel.setAlwaysOnTop(true);
    
    
    }
    
    public void FrontdeskPanelActivation(){
        frontdeskPanel_IB.Init_Behaviour();
    }
    
    public void DoctorPanelActivation() {
        activePanel = new DoctorPanel();
        loginPanel.setVisible(false);
        activePanel.setVisible(true);
        activePanel.setAlwaysOnTop(true);
    
    }
    
    public void PharmacistPanelActivation(){
        activePanel = new PharmacistPanel();
        loginPanel.setVisible(false);
        activePanel.setVisible(true);
        activePanel.setAlwaysOnTop(true);
        
    }
    
    
}
