package Patient;

import java.sql.Date;

public class Patient {
    
    private final int id;
    private final String local_id;
    private final String bpjs_id;
    private final String fullname;
    private final Date date_ofbirth;
    private final String place_ofbirth;
    private final char sex;
    private final String address;
    private final String firm;
    private final String contact;
    public static int totalColumn = 10;
    
//    public Patient(int id, String bpjs_id, String fullname)
//    {
//        this.id = id;
//        this.local_id =  null;
//        this.bpjs_id = bpjs_id;
//        this.fullname = fullname;
//        this.date_ofbirth = null;   
//    }
    
    
    public Patient(int id, String local_id, 
            String bpjs_id, String fullname, 
            Date date_ofbirth, String place_ofbirth,
            char sex, String address, 
            String firm, String contact)
    {
        this.id = id;
        this.local_id = local_id;
        this.bpjs_id = bpjs_id;
        this.fullname = fullname;
        this.date_ofbirth = date_ofbirth;   
        this.place_ofbirth = place_ofbirth;
        this.sex = sex;
        this.address = address;
        this.firm = firm;
        this.contact = contact;
    }
    
    public int Get_id(){ return id; }
    public String Get_local_id(){ return local_id; }
    public String Get_bpjs_id(){ return bpjs_id; }
    public String Get_fullname(){ return fullname; }
    public Date Get_date_ofbirth(){ return date_ofbirth; }
    public String Get_place_ofbirth() { return place_ofbirth; }
    public char Get_sex() { return sex; }
    public String Get_address() { return address; }
    public String Get_Firm() { return firm; }
    public String Get_Contact() { return contact; }
    
    
}
